<?php

class TemplateAPIExportUI extends ctools_export_ui {

  /**
   * Menu callback to determine if an operation is accessible.
   *
   * This function enforces a basic access check on the configured perm
   * string, and then additional checks as needed.
   *
   * @param $op
   *   The 'op' of the menu item, which is defined by 'allowed operations'
   *   and embedded into the arguments in the menu item.
   * @param $item
   *   If an op that works on an item, then the item object, otherwise NULL.
   *
   * @return
   *   TRUE if the current user has access, FALSE if not.
   */
  function access($op, $item) {
    $base = 'template api ';

    if (!user_access($this->plugin['access'])) {
      return FALSE;
    }

    if ($op !== 'list' && !user_access($base . $op)) {
      return FALSE;
    }

    // If we need to do a token test, do it here.
    if (!empty($this->plugin['allowed operations'][$op]['token']) && (!isset($_GET['token']) || !drupal_valid_token($_GET['token'], $op))) {
      return FALSE;
    }

    switch ($op) {
      case 'revert':
        return ($item->export_type & EXPORT_IN_DATABASE) && ($item->export_type & EXPORT_IN_CODE);
      case 'delete':
        return ($item->export_type & EXPORT_IN_DATABASE) && !($item->export_type & EXPORT_IN_CODE);
      case 'disable':
        return empty($item->disabled);
      case 'enable':
        return !empty($item->disabled);
      default:
        return TRUE;
    }
  }

  /**
   * Handle the submission of the edit form.
   *
   * At this point, submission is successful. Our only responsibility is
   * to copy anything out of values onto the item that we are able to edit.
   *
   * If the keys all match up to the schema, this method will not need to be
   * overridden.
   */
  function edit_form_submit(&$form, &$form_state) {
    $form_state['item']->name = $form_state['values']['name'];
    $form_state['item']->label = $form_state['values']['label'];
    $form_state['item']->description = $form_state['values']['description'];
    $form_state['item']->tags = empty($form_state['values']['tags']) ? array() :
      explode(',', $form_state['values']['tags']);
    $form_state['item']->renderer = $form_state['values']['renderer'];

    $form_state['item']->content = $form_state['values']['content']['value'];

    $input_values = array();
    foreach ($form_state['values']['inputs'] as $input_key => $input_definition) {
      if (empty($input_definition['remove'])) {
        if ($input_key === 'new') {
          if (!empty($input_definition['key'])) {
            $key = $input_definition['key'];
          }
          else {
            $key = drupal_clean_css_identifier($input_definition['label']);
          }
          if (!empty($key)) {
            $input_values[$key] = $input_definition;
          }
        }
        else {
          $input_values[$input_key] = $input_definition;
        }
      }
    }

    $form_state['item']->inputs = $input_values;

    $attached = $form_state['values']['attached'];
    $attached_values = array();

    $attached_values['js'] = $attached['js']['js_content']['value'];
    $attached_values['css'] = $attached['css']['css_content']['value'];

    $library_values = array_filter($attached['libraries']['libraries']);
    $libraries = array();
    foreach ($library_values as $value) {
      if (!empty($value)) {
        list($module, $library_name) = explode(':::', $value);
        $libraries[] = array('name' => $library_name, 'module' => $module);
      }
    }

    $attached_values['libraries'] = $libraries;

    $form_state['item']->attached = $attached_values;
  }
}
