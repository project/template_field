<?php

$plugin = array(
  'schema' => 'template',
  'menu' => array(
    'menu prefix' => 'admin/structure',
    'menu item' => 'template',
    'menu title' => 'Templates',
    'menu description' => 'Manage the Site Templates.',
  ),
  'title singular' => t('template'),
  'title singular proper' => t('Template'),
  'title plural' => t('templates'),
  'title plural proper' => t('Templates'),
  'form' => array(
    'settings' => 'templates_form',
  ),
  'handler' => array(
    'class' => 'TemplateAPIExportUI',
    'parent' => 'ctools_export_ui',
  ),
  'access' => 'access template administration pages',
);

function templates_form(&$form, $form_state) {
  $template = $form_state['item'];
  unset($form['info']);

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#maxlength' => 60,
    '#default_value' => isset($template->label) ? $template->label : '',
  );

  $form['name'] = array(
    '#title' => t('Unique name for the template'),
    '#type' => 'machine_name',
    '#maxlength' => 32,
    '#default_value' => isset($template->name) ? $template->name : '',
    '#machine_name' => array(
      'exists' => 'template_api_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this template. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => isset($template->description) ? $template->description : '',
  );

  $form['tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Tags'),
    '#default_value' => isset($template->tags) ? implode(',', $template->tags) : '',
    '#description' => t('A comma seperated list of tags'),
  );

  $renderers = template_api_get_renderers();
  if (count($renderers) > 1) {
    $options = array();
    foreach($renderers as $renderer) {
      $options[$renderer['name']] = $renderer['label'];
    }
    $form['renderer'] = array(
      '#title' => t('Renderer'),
      '#type' => 'select',
      '#default_value' => isset($template->renderer) ? $template->renderer : '',
      '#options' => $options,
    );
  }
  else {
    $renderer = reset($renderers);
    $form['renderer'] = array(
      '#type' => 'value',
      '#value' => $renderer['name'],
    );
  }

  $form['settings-tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content'),
    '#group' => 'settings-tabs',
    '#tree' => 0,
    '#weight' => 1,
  );
  $form['content']['content'] = array(
    '#title' => t('Content of template'),
    '#type' => 'text_format',
    '#default_value' => isset($template->content) ? $template->content : '',
    '#rows' => 20,
    '#format' => 'filtered_html',
  );

  $form['attached'] = array(
    '#type' => 'container',
    '#title' => t('Attachments'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $form['attached']['css'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSS'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'settings-tabs',
    '#weight' => 2,
  );

  $form['attached']['css']['css_content'] = array(
    '#title' => t('CSS'),
    '#type' => 'text_format',
    '#default_value' => isset($template->attached['css']) ? $template->attached['css'] : '',
    '#rows' => 10,
    '#format' => 'filtered_html',
  );

  $form['attached']['js'] = array(
    '#type' => 'fieldset',
    '#title' => t('JavaScript'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'settings-tabs',
    '#weight' => 3,
  );

  $form['attached']['js']['js_content'] = array(
    '#title' => t('JavaScript'),
    '#type' => 'text_format',
    '#default_value' => isset($template->attached['js']) ? $template->attached['js'] : '',
    '#rows' => 10,
    '#format' => 'filtered_html',
  );

  $form['attached']['libraries'] = array(
    '#title' => t('Libraries'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'settings-tabs',
    '#weight' => 4,
  );

  // Attached files
  $libraries = array();
   foreach (module_list() as $module) {
     foreach (drupal_get_library($module) as $library_key => $library_data) {
       $libraries["$module:::$library_key"] = $library_data['title'];
     }
   }

  if (!isset($template->attached['libraries'])) {
    $template->attached['libraries'] = array();
  }

  $default_values = array();
  foreach($template->attached['libraries'] as $key => $attached) {
    $default_values[] = "{$attached['module']}:::{$attached['name']}";
  }

  $form['attached']['libraries']['libraries'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Additional libraries'),
    '#options' => $libraries,
    '#default_value' => drupal_map_assoc($default_values),
  );

  $form['inputs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Input Definitions'),
    '#group' => 'settings-tabs',
    '#weight' => 5,
    '#tree' => TRUE,
    '#theme' => 'template_api_template_inputs_form',
  );

  $inputs_types = template_api_get_input_types();
  $options = array();
  foreach($inputs_types as $input_type) {
    $options[$input_type['name']] = $input_type['label'];
  }

  if (!isset($template->inputs)) {
    $template->inputs = array();
  }
  foreach ($template->inputs as $input_key => $input) {
    $form['inputs'][$input_key] = array(
      '#type' => 'container',
      '#title' => $input['label'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );

    $form['inputs'][$input_key]['label'] = array(
      '#title' => t('Label'),
      '#type' => 'textfield',
      '#default_value' => $input['label'],
      '#size' => '20',
    );

    $form['inputs'][$input_key]['type'] = array(
      '#title' => t('Type'),
      '#type' => 'select',
      '#options' => $options,
      '#empty_option' => t('- Select an input type -'),
      '#default_value' => $input['type'],
    );

    $form['inputs'][$input_key]['remove'] = array(
      '#title' => t('Remove this input?'),
      '#type' => 'checkbox',
      '#default_value' => FALSE,
    );

    $form['inputs'][$input_key] += $template->getExtraAdminFields($input['type'], $input);
  }

  $form['inputs']['new'] = array(
    '#type' => 'container',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $form['inputs']['new']['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Add new input'),
    '#default_value' => '',
    '#size' => '20',
  );

  $form['inputs']['new']['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => '',
    '#size' => '20',
  );

  $form['inputs']['new']['type'] = array(
    '#title' => t('Type'),
    '#type' => 'select',
    '#options' => $options,
    '#empty_option' => t('- Select an input type -'),
    '#default_value' => '',
  );
}

function theme_template_api_template_inputs_form($variables) {
  $form = $variables['form'];

  $rows = array();
  $header = array('key', 'label', 'type', 'remove', 'extra');

  foreach (element_children($form) as $key) {
    $form[$key]['label']['#title_display'] = 'invisible';
    $form[$key]['type']['#title_display'] = 'invisible';
    $form[$key]['remove']['#title_display'] = 'invisible';

    $row = array(
      isset($form[$key]['key']) ? drupal_render($form[$key]['key']) : check_plain($key),
      drupal_render($form[$key]['label']),
      drupal_render($form[$key]['type']),
      drupal_render($form[$key]['remove']),
      drupal_render($form[$key]),
    );
    $rows[] = $row;
  }
  $form[] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#header' => $header,
  );

  return drupal_render_children($form);
}
