<?php

class TemplateApiColorInputType extends TemplateApiInputType {
  public function form($input_definition, $input_values) {
    $form = array();

    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => $input_definition['label'],
      '#default_value' => isset($input_values['url']) ? $input_values['url'] : '',
    );

    $form['options'] = array(
      '#tree' => TRUE
    );

    $form['options']['absolute'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($input_values['options']['absolute']) ? $input_values['options']['absolute'] : FALSE,
      '#label' => t('Absolute'),
    );

    return $form;
  }

  public function getVariable($input_definition, $input_values) {
    if (isset($input_values['options']) && is_array($input_values['options'])) {
      $options = $input_values['options'];
    }
    else {
      $options = array();

    }
    return url($input_values['url'], $options);
  }

  public function isEmpty($input_definition, $input_values) {
    return empty($input_values['url']);
  }
}